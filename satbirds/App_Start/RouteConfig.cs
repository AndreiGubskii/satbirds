﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace satbirds
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                name: "Tours",
                url: "tours/{language}/{days}",
                defaults: new { controller = "Home", action = "Tours", language = "en", days = 7 }
            );

            routes.MapRoute(
                name: "Books",
                url: "books/{language}",
                defaults: new { controller = "Home", action = "Books", language = "en" }
            );

            routes.MapRoute(
                name: "Book",
                url: "book/{language}/{id}",
                defaults: new { controller = "Home", action = "Book", language = "en", id = "1" }
            );

            routes.MapRoute(
                name: "AboutUs",
                url: "aboutus/{language}",
                defaults: new { controller = "Home", action = "AboutUs", language = "en" }
            );
            routes.MapRoute(
                name: "PrivacyPolicy",
                url: "privacypolicy",
                defaults: new { controller = "Home", action = "PrivacyPolicy", id = "en" }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = "en" }
            );

        }
    }
}
