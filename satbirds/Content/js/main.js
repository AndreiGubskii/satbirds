﻿document.addEventListener("touchstart", function () { }, false);
var menu = $(".nav-wrapper"),
    menuBtn = $("#menu_btn"),
    mainMenu = $(".main-nav");

menuBtn.click(function () {
    if (mainMenu.is(":hidden")) {
        $(".main-nav").show("slow");
    }
});

$(document).mousedown(function (e) {
    if (mainMenu.has(e.target).length === 0 && !menuBtn.is(":hidden")) {
        mainMenu.hide("slow");
    }
});

$(window).resize(function () {
    if (menuBtn.is(":hidden")) {
        mainMenu.css("display","inline-block");
    } else {
        mainMenu.css("display", "none");
    }
});



// Плавная прокрутка по якорям
menu.on("click",
    "#main-nav_item",
    function (event) {
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();

        //забираем идентификатор бока с атрибута href
        var id = $(this).attr('href'),

            //узнаем высоту от начала страницы до блока на который ссылается якорь
            top = $(id).offset().top - 100;

        //анимируем переход на расстояние - top за 700 мс
        $('body,html').animate({ scrollTop: top }, 700);
    });

// Sticky Header

$(window).scroll(function () {
    if ($(window).scrollTop() >= 0.5) {
        $('.nav-wrapper').addClass('sticky');
    } else {
        $('.nav-wrapper').removeClass('sticky');
    }
});


// **** fancybox  ******
$(document).ready(function () {
    $(".fancybox").fancybox();

    $("#phone").intlTelInput({
        initialCountry: "auto",
        geoIpLookup: function (callback) {
            $.get('http://ipinfo.io', function () { }, "jsonp").always(function (resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
        utilsScript: "../../Content/Vendor/InternationalTelephoneInput/js/utils.js" // just for formatting/placeholders etc
    });
});

//FormListeners

function OnBeginMailSent() {
    //alert("OnBegin");
}

function OnSuccessMailSent(data) {
    //alert("OnSuccess: " + data);
    alert("Your message was successfully sent!");
}

function OnFailureMailSent(request, error) {
    //alert("OnFailure: " + error);
    alert("Message could not be sent!");
}

function OnCompleteMailSent(request, status) {
    //Сбрасываем все поля
    var mailForm = $("#mailForm");
    for (var i = 0; i < mailForm.length; i++) {
        mailForm[i].reset();
    }
}