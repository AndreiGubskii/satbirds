﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace satbirds.Models
{
    public class MailModel
    {
        public string Body { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
    }
}