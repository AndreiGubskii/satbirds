﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using satbirds.Models;

namespace satbirds.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index() {
            return View();
        }

        public ActionResult Books(string language) {
            if (language == "ru")            {
                return View("Books-RU");
            }
            return View();
        }
        public ActionResult Book(string language,int id) {
            if (id == null) {
                id = 1;
            }
            if (language == "ru") {
                return View("Book-RU_"+id);
            }
            return View("Book_"+id);
        }

        public ActionResult Tours(string language,int days) {
            if (language == "ru"){
                if (days == 10) {
                    return View("Tours_10-RU");
                }
                return View("Tours_7-RU");
            }

            if (days == 10)
            {
                return View("Tours_10");
            }
            return View("Tours_7");
        }

        public ActionResult AboutUs(string language) {
            if (language == "ru"){
                return View("AboutUs-RU");
            }
            return View();
        }
        [HttpPost]
        public ActionResult SendMail(MailModel model)
        {
            MailController.SendMail(model);
            return View("Tours_7");
        }

        public ActionResult PrivacyPolicy() {
            return View();
        }
    }
}