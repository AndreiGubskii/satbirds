﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Authentication;
using System.Web;
using System.Web.Mvc;
using satbirds.Models;

namespace satbirds.Controllers
{
    public class MailController : Controller
    {
        public static void SendMail(MailModel model) {

            string ToAddress = ConfigurationManager.AppSettings["toMailAccount"];
            string FromAddress = ConfigurationManager.AppSettings["mailAccount"];
            string Subject = string.Format("{0} Сообщение с сайта satbirds.com", model.Name);

            // Create the MailMessage object
            MailMessage mm = new MailMessage(FromAddress, ToAddress);
            mm.Subject = Subject;
            mm.IsBodyHtml = true;
            mm.Priority = MailPriority.High;
            mm.Body = string.Format(@"
<html>
<body>
  <h1>Сообщение с сайта satbirds.com</h1>
  <table cellpadding=""5"" cellspacing=""0"" border=""1"">
  <tr>
  <td style=""text-align: right;font-weight: bold"">Имя пользователя:</td>
  <td>{0}</td>
  </tr>
  <tr>
  <td style=""text-align: right;font-weight: bold"">E-mail пользователя:</td>
  <td>{1}</td>
  </tr>
  <tr>
  <td style=""text-align: right;font-weight: bold"">Телефон пользователя:</td>
  <td>{2}</td>
  </tr>
  <tr>
  <td style=""text-align: right;font-weight: bold"">Сообщение:</td>
  <td>{3}</td>
  </tr>
  </table>
</body>
</html>",
                model.Name,
                model.Email,
                model.Phone,
                model.Body
                );

            SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["mailHost"], Int32.Parse(ConfigurationManager.AppSettings["mailHostPort"]));
            client.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["mailAccount"], ConfigurationManager.AppSettings["mailPassword"]);
            client.EnableSsl = true;
            client.Send(mm);

        }

    }
}